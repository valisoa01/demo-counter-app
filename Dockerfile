#FROM maven:onbuild AS buildenv

#WORKDIR /opt/app

#COPY ./ /opt/app

#RUN mvn clean install -DskipTests

#FROM openjdk:jre-alpine

# COPY --from=buildenv /opt/app/target/springboot-1.0.0.jar /app.jar
#COPY --from=buildenv /opt/app/target/demo-counter-app.jar /app.jar

#ENV PORT 8087
#EXPOSE $PORT

#ENTRYPOINT ["java", "-jar", "-Xmx1024M", "-Dserver.port=${PORT}", "app.jar"]
#CMD ["java", "-jar", "/app.jar"]


FROM maven as build
WORKDIR /app
COPY . .
RUN mvn install 

FROM openjdk:11.0
WORKDIR /app
COPY --from=build /app/target/Uber.jar /app/
expose 8087
ENTRYPOINT ["java", "-jar", "/app/Uber.jar"]
#CMD ["java", "-jar", "/Uber.jar"]
